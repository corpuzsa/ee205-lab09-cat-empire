///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Shaun Corpuz <corpuzsa@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   04/22/2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

bool CatEmpire::empty(){
   return (topCat == nullptr);
}

// addCat public
void CatEmpire::addCat( Cat* newCat ) {
   if( topCat == nullptr ){
      topCat = newCat;
      return;
   }

   else{
      addCat( topCat, newCat );
   }
}

// addCat private
void CatEmpire::addCat( Cat* atCat, Cat* newCat ){
   if( atCat->name > newCat->name){
      
      if( atCat->left == nullptr ){
         atCat->left = newCat;
      }

      else{
         addCat( atCat->left, newCat);
      }
   }

   if( atCat->name < newCat->name ){

      if( atCat->right == nullptr ){
         atCat->right = newCat;
      }

      else{
         addCat( atCat->right, newCat );
      }
   }

   return;
}

// catFamilyTree
void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsInorderReverse( topCat, 1);

}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const{
   if( atCat == nullptr )
      return;
  
   if( atCat->right != nullptr ){
      dfsInorderReverse( atCat->right, depth+1 );
   }

   cout << string(6 * (depth-1), ' ') << atCat->name;

   if( atCat->left == nullptr && atCat->right == nullptr ){
      cout << endl;
   }

   if( atCat->left != nullptr && atCat->right != nullptr ){
      cout << "<" << endl;
   }

   if( atCat->left != nullptr && atCat->right == nullptr ){
      cout << "\\" << endl;
   }

   if( atCat->left == nullptr && atCat->right != nullptr ){
      cout << "/" << endl;
   }

   if( atCat->left != nullptr ){
      dfsInorderReverse( atCat->left, depth+1 );
   }
}

// catList
void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsInorder( topCat );
}

void CatEmpire::dfsInorder( Cat* atCat) const{
   if( atCat == nullptr )
      return;

   dfsInorder( atCat->left );

   cout << atCat->name << endl;

   dfsInorder( atCat->right );

}

// catBegat
void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsPreorder( topCat );
}

void CatEmpire::dfsPreorder( Cat* atCat) const{
   if( atCat == nullptr )
      return;

   if( atCat->left != nullptr && atCat->right != nullptr ){
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   }

   if( atCat->left != nullptr && atCat->right == nullptr ){
      cout << atCat->name << " begat " << atCat->left->name << endl;
   }

   if( atCat->left == nullptr && atCat->right != nullptr ){
      cout << atCat->name << " begat " << atCat->right->name << endl;
   }

   dfsPreorder( atCat->left );
   dfsPreorder( atCat->right );

}

void CatEmpire::catGenerations() const{
   int gen = 1;

   queue<Cat*> catQueue;
   catQueue.push(nullptr);
   catQueue.push(topCat);

   while(catQueue.size() > 1){
      Cat* cat = catQueue.front();
      catQueue.pop();

      //On a new generation
      if(cat == nullptr){
         if(gen != 1)
            cout << endl;
         getEnglishSuffix(gen);
         cout << " Generation" << endl;
         cout << "  ";
         gen++;
         catQueue.push(nullptr);
         continue;
      }

      cout << cat->name << " ";

      if(cat == nullptr)
         return;

      if(cat->left != nullptr){
         catQueue.push(cat->left);
      }

      if(cat->right != nullptr){
         catQueue.push(cat->right);
      }
   }
   cout << endl;

   // empty the queue
   while(!catQueue.empty()){
      catQueue.pop();
   }

}

void CatEmpire::getEnglishSuffix(int n) const{

   //Looking at last 2 digits
   if(n%100 < 11 || n%100 > 13){
      //Looking at last digit
      switch(n%10){
         case 1:
            cout << n << "st";
            break;
         case 2:
            cout << n <<  "nd";
            break;
         case 3:
            cout << n <<  "rd";
            break;
         default:
            cout << n <<  "th";
            break;
      }
   }else{
      cout << n <<  "th";
   }
}


